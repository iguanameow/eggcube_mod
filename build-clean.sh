cd bin_unix
rm -rf  eggcube*
cd ../source/enet
make clean
make distclean
rm -rf autom4te.cache
cd ../src
make clean
rm -rf *.cpp~ *.h~
cd ../../
echo "Cleaning Done!"
