#!/bin/bash
sudo apt update -y
sudo apt install -y   \
  git                 \
  build-essential     \
  zlib1g              \
  zlib1g-dev          \
  clang               \
  curl                \
  libsdl1.2debian     \
  libsdl-gfx1.2-dev   \
  libsdl1.2-dev       \
  libsdl-image1.2     \
  libsdl-image1.2-dev \
  libsdl-mixer1.2     \
  libsdl-mixer1.2-dev \
  libopenal1          \
  libopenal-dev       \
  libvorbis-dev       \
  libcurl3            \
  libcurl4-openssl-dev
