ConcurProc=$(CONCURRENCY_LEVEL=$(getconf _NPROCESSORS_ONLN))
NumProc=$(($(getconf _NPROCESSORS_ONLN)+1))
./build-clean.sh
cd source/src/
$ConcurProc make -j $NumProc client
$ConcurProc make -j $NumProc server
$ConcurProc make -j $NumProc master
#make install
chmod +x eggcube_*
mv eggcube_* ../../bin_unix/
cd ../../
chmod +x *.sh
sed -i -e 's/\r$//' *.sh
echo "[-------- Building Done! --------]"
echo "[-- Binaries found in bin_unix --]"
