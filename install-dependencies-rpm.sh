#!/bin/bash
sudo dnf update -y
sudo dnf install -y   	\
  git					\
  make					\
  gcc-c++				\
  clang					\
  SDL* 					\
  zlib					\
  zlib-devel			\
  openal				\
  openal-devel			\
  libogg				\
  libvorbis				\
  libcurl				\
  libcurl-devel			\
  openssl-devel
